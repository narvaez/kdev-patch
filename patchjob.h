/*
 * <one line to give the program's name and a brief idea of what it does.>
 * Copyright (C) 2012  David E. Narváez <david@li177-19.members.linode.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef PATCHPLUGIN_PATCHJOB_H
#define PATCHPLUGIN_PATCHJOB_H

#include <../kdevplatform/outputview/outputexecutejob.h>

class PatchJob : public KDevelop::OutputExecuteJob
{
    Q_OBJECT

public:
    PatchJob(const QString & title, QObject* parent = 0, OutputJobVerbosity verbosity = OutputJob::Verbose);

};

#endif // PATCHPLUGIN_PATCHJOB_H
