#include "patchplugin.h"
#include "ui_applypatch.h"
#include "patchjob.h"

#include <limits>

#include <interfaces/icore.h>
#include <interfaces/iuicontroller.h>
#include <interfaces/iprojectcontroller.h>
#include <interfaces/context.h>
#include <interfaces/contextmenuextension.h>
#include <interfaces/iproject.h>
#include <interfaces/isession.h>
#include <interfaces/iruncontroller.h>
#include <project/projectmodel.h>
#include <outputview/outputexecutejob.h>

#include <kompare/kompareinterface.h>

#include <KPluginFactory>
#include <KService>
#include <KLocalizedString>
#include <KAboutData>
#include <KParts/MainWindow>
#include <KIO/NetAccess>
#include <KDebug>

#include <QAction>

K_PLUGIN_FACTORY(PatchFactory, registerPlugin<PatchPlugin>(); )
K_EXPORT_PLUGIN(PatchFactory(
                    KAboutData("kdevpatch","kdevpatch",
                               ki18n("Patch"), "0.1", ki18n("Apply patches on folders"), KAboutData::License_GPL)))

PatchPlugin::PatchPlugin(QObject* parent, const QVariantList&)
    : KDevelop::IPlugin(PatchFactory::componentData(), parent)
{
}

KDevelop::ContextMenuExtension PatchPlugin::contextMenuExtension(KDevelop::Context* context)
{
    KDevelop::ContextMenuExtension ext;

    if(context->type() == KDevelop::Context::ProjectItemContext)
    {
        KDevelop::ProjectItemContext* pic = dynamic_cast<KDevelop::ProjectItemContext*>(context);

        if(!pic->items().empty())
        {
            m_currentPath = pic->items().first()->url();

            QFileInfo dirInfo(m_currentPath.toLocalFile());

            if(dirInfo.isDir())
            {
                QAction* patchAction = new QAction(KIcon("text-x-patch"), "Apply Patch", this);

                connect(patchAction, SIGNAL(triggered(bool)), this, SLOT(callPatchUI()));
                ext.addAction(KDevelop::ContextMenuExtension::FileGroup, patchAction);
            }
        }
    }

    return ext;
}

void PatchPlugin::callPatchUI()
{
    KDevelop::IUiController* ui = KDevelop::ICore::self()->uiController();
    QDialog dialog(ui->activeMainWindow());
    KService::Ptr kompareService = KService::serviceByDesktopName("komparepart");
    KService::Ptr kompareNavService = KService::serviceByDesktopName("komparenavtreepart");

    m_applyPatchUI.setupUi(&dialog);

    if(!kompareService || !kompareNavService)
    {
        return;
    }

    KParts::ReadOnlyPart * komparePart = kompareService->createInstance<KParts::ReadOnlyPart>(this);
    KParts::ReadOnlyPart * kompareNavPart = kompareNavService->createInstance<KParts::ReadOnlyPart>(this);

    if(!komparePart || !kompareNavPart)
    {
        return;
    }

    m_kompareInterface = qobject_cast< KompareInterface* >(komparePart);
    m_applyPatchUI.widgetsLayout->insertWidget(3, komparePart->widget(), std::numeric_limits< int >::max());
    kompareNavPart->widget()->setMaximumHeight(60);
    m_applyPatchUI.navTreeLayout->addWidget(kompareNavPart->widget());
    connect(m_applyPatchUI.patchUrlRequester, SIGNAL(urlSelected(KUrl)), this, SLOT(changePatchURL(KUrl)));

    // Workaround for the lack of button roles in the designer
    foreach(QAbstractButton * b, m_applyPatchUI.buttonBox->buttons())
        if(m_applyPatchUI.buttonBox->standardButton(b) == QDialogButtonBox::Apply)
        {
            m_applyPatchUI.buttonBox->addButton(b, QDialogButtonBox::AcceptRole);
            b->setEnabled(false);

            break;
        }

    connect(komparePart, SIGNAL(modelsChanged(const Diff2::DiffModelList*)), kompareNavPart, SLOT( slotModelsChanged( const Diff2::DiffModelList*) ) );
    connect(komparePart, SIGNAL(kompareInfo(Kompare::Info*)), kompareNavPart, SLOT( slotKompareInfo(Kompare::Info*)));
    connect(kompareNavPart, SIGNAL(selectionChanged(const Diff2::DiffModel*, const Diff2::Difference*)), komparePart, SIGNAL(selectionChanged(const Diff2::DiffModel*, const Diff2::Difference*)));
    connect(komparePart, SIGNAL(setSelection(const Diff2::DiffModel*, const Diff2::Difference*)), kompareNavPart, SLOT(slotSetSelection(const Diff2::DiffModel*, const Diff2::Difference*)));
    connect(kompareNavPart, SIGNAL(selectionChanged(const Diff2::Difference*)), komparePart, SIGNAL(selectionChanged(const Diff2::Difference*)));
    connect(komparePart, SIGNAL(setSelection(const Diff2::Difference*)), kompareNavPart, SLOT( slotSetSelection(const Diff2::Difference*)));
    connect(komparePart, SIGNAL(applyDifference(bool)), kompareNavPart, SLOT(slotApplyDifference(bool)));
    connect(komparePart, SIGNAL(applyAllDifferences(bool)), kompareNavPart, SLOT(slotApplyAllDifferences(bool)));
    connect(komparePart, SIGNAL(applyDifference(const Diff2::Difference*, bool)), kompareNavPart, SLOT(slotApplyDifference(const Diff2::Difference*, bool)));

    if(dialog.exec() == QDialog::Accepted)
    {
        KUrl patchUrl = m_applyPatchUI.patchUrlRequester->url();
        QString localPatchUrl;
        PatchJob * patchJob = new PatchJob(patchUrl.isLocalFile() ? patchUrl.toLocalFile() : patchUrl.url(), this);

        if(patchUrl.isLocalFile())
        {
            localPatchUrl = patchUrl.toLocalFile();
        }
        else
        {
            KIO::NetAccess::download(patchUrl, localPatchUrl, KDevelop::ICore::self()->uiController()->activeMainWindow());
        }

        patchJob->setWorkingDirectory(m_currentPath);
        *patchJob << "patch" << "-t" << "-p" << QString::number(m_applyPatchUI.stripPathSpin->value()) << "-i" << localPatchUrl;
        KDevelop::ICore::self()->runController()->registerJob(patchJob);
    }

    return;
}

void PatchPlugin::changePatchURL(const KUrl& url)
{
    foreach(QAbstractButton * b, m_applyPatchUI.buttonBox->buttons())
        b->setEnabled(url.isValid());

    if(m_kompareInterface)
    {
        m_kompareInterface->openDiff(url);
    }
}
