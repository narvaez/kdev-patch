#ifndef PATCH_PLUGINPLUGIN_H
#define PATCH_PLUGINPLUGIN_H

#include <interfaces/iplugin.h>

#include <QVariant>
#include <QStandardItemModel>

#include <KUrl>
#include <ui_applypatch.h>

class KompareInterface;

class PatchPlugin : public KDevelop::IPlugin
{
    Q_OBJECT
    
public:
    PatchPlugin(QObject *parent, const QVariantList & args);
    KDevelop::ContextMenuExtension contextMenuExtension( KDevelop::Context* context );
    
protected slots:
    void callPatchUI();
    void changePatchURL(const KUrl & url);
    
private:
    KUrl m_currentPath;
    KompareInterface * m_kompareInterface;
    Ui::ApplyPatch m_applyPatchUI;
};

#endif
