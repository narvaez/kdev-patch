project(patch)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
                      ${CMAKE_CURRENT_SOURCE_DIR}/cmake/modules
)

find_package(KDE4 REQUIRED)
find_package(KDevPlatform REQUIRED)

include_directories(
    ${KDE4_INCLUDES}
    ${KDEVPLATFORM_INCLUDE_DIR}
    ${CMAKE_CURRENT_BINARY_DIR}
    ${CMAKE_CURRENT_SOURCE_DIR}
    ${CMAKE_SOURCE_DIR}
)

set(patch_SRCS
        patchplugin.cpp
        patchjob.cpp
)

kde4_add_ui_files(patch_SRCS applypatch.ui)

kde4_add_plugin(kdevpatchplugin ${patch_SRCS})

# Find kompare
find_file(KOMPARE_INTERFACE_H kompare/kompareinterface.h)

if(KOMPARE_INTERFACE_H MATCHES KOMPARE_INTERFACE_H-NOTFOUND)
    message(SEND_ERROR "kompare/kompareinterface.h not found")
    return()
endif(KOMPARE_INTERFACE_H MATCHES KOMPARE_INTERFACE_H-NOTFOUND)

find_library(KOMPARE_INTERFACE_LIBRARY kompareinterface)

if(KOMPARE_INTERFACE_LIBRARY MATCHES KOMPARE_INTERFACE_LIBRARY-NOTFOUND)
    message(SEND_ERROR "libkompareinterface.so not found")
    return()
endif(KOMPARE_INTERFACE_LIBRARY MATCHES KOMPARE_INTERFACE_LIBRARY-NOTFOUND)

target_link_libraries(kdevpatchplugin
    ${KDEVPLATFORM_INTERFACES_LIBRARIES}
    ${KDEVPLATFORM_LANGUAGE_LIBRARIES}
    ${KDEVPLATFORM_PROJECT_LIBRARIES}
    ${KDEVPLATFORM_OUTPUTVIEW_LIBRARIES}
    kompareinterface
)

install(TARGETS kdevpatchplugin DESTINATION ${PLUGIN_INSTALL_DIR})

install(FILES kdevpatchplugin.desktop DESTINATION ${SERVICES_INSTALL_DIR})

 
